TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    member.cpp \
    book.cpp \
    Reception.cpp

HEADERS += \
    member.h \
    book.h \
    Reception.h
